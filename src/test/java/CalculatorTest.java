import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void addMethod() {
        Calculator calculator=new Calculator();
        Assert.assertEquals(4,calculator.addMethod(2, 2),0);

    }

    @Test
    public void subMethod() {
        Calculator calculator=new Calculator();
        Assert.assertEquals(0,calculator.subMethod(2, 2),0);

    }

    @Test
    public void multiplyMethod() {
        Calculator calculator=new Calculator();
        Assert.assertEquals(4,calculator.multiplyMethod(2, 2),0);

    }

    @Test
    public void divisionMethod() {
        Calculator calculator=new Calculator();
        Assert.assertEquals(1,calculator.divisionMethod(2, 2),0);

    }



}