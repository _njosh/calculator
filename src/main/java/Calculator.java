

import javax.swing.*;
import java.awt.event.*;

public class Calculator {
    private double num1;
    private double num2;
    private double result;
    private int selection;
    private JMenuBar menuBar=new JMenuBar();
    private JPanel Calculator;//panel calculator for textfield and all buttons
    private JLabel label;
    private JTextField text;
    private JButton btnDivide;
    private JButton btnMultiply;
    private JButton btnSub;
    private JButton btnOne;
    private JButton btnTwo;
    private JButton btnFour;
    private JButton btnSeven;
    private JButton btnZero;
    private JButton btnThree;
    private JButton btnFive;
    private JButton btnEight;
    private JButton btnReset;
    private JButton btnAdd;
    private JButton btnSix;
    private JButton btnNine;
    private JButton btnEquals;
    private JButton buttonPoint;





    public Calculator() {




        btnOne.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { //actionPerformed method in an interface ActionListener
                String btnOneText=text.getText()+btnOne.getText();//text field string adding with btnOne string, so++ it will add 1 to the text field
                text.setText(btnOneText);
               //+ System.out.println(btnOneText);
            }
        });
        btnTwo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnTwoText=text.getText()+btnTwo.getText();
                text.setText(btnTwoText);
            }
        });
        btnThree.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnThreeText=text.getText()+btnThree.getText();
                text.setText(btnThreeText);
            }
        });
        btnFour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnFourText=text.getText()+btnFour.getText();
                text.setText(btnFourText);
            }
        });
        btnFive.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnFiveText=text.getText()+btnFive.getText();
                text.setText(btnFiveText);
                            }
        });
        btnSix.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnSixText=text.getText()+btnSix.getText();
                text.setText(btnSixText);
            }
        });
        btnSeven.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnSevenText=text.getText()+btnSeven.getText();
                text.setText(btnSevenText);
            }
        });
        btnEight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnEightText=text.getText()+btnEight.getText();
                text.setText(btnEightText);
            }
        });
        btnNine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnNineText=text.getText()+btnNine.getText();
                text.setText(btnNineText);
            }
        });
        btnZero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnZeroText=text.getText()+btnZero.getText();
                text.setText(btnZeroText);
            }
        });
        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                label.setText("");
                buttonPoint.setEnabled(true);


            }
        });
        buttonPoint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String btnPointText=text.getText()+buttonPoint.getText();
                text.setText(btnPointText);
                buttonPoint.setEnabled(false);//button point cannot be pressed twice
            }
        });

        text.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c=e.getKeyChar();//when entered value from key checks digit and point and equals
                if(!(Character.isDigit(c)||(c==KeyEvent.VK_PERIOD))) {
                    e.consume();
                   if(c==KeyEvent.VK_ADD||c==KeyEvent.VK_PLUS||c==43)
                   {
                       btnAdd.doClick();
                   }
                   else if(c==KeyEvent.VK_SUBTRACT||c==KeyEvent.VK_MINUS)
                   {
                       btnSub.doClick();
                   }
                   else if(c==KeyEvent.VK_MULTIPLY||c==42)
                   {
                       btnMultiply.doClick();
                   }
                   else if(c==KeyEvent.VK_DIVIDE||c==47)
                   {
                       btnDivide.doClick();
                   }
                   else if (c==KeyEvent.VK_EQUALS||c==10){
                       btnEquals.doClick();
                   }
                }



            }
        });



        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                   try {

                       String str = text.getText();
                       num1 = Double.parseDouble(text.getText());
                       text.setText("");
                       label.setText(str + "+");
                       buttonPoint.setEnabled(true);//after the add sign button point can be pressed
                       selection = 1;
                   }
                   catch (NumberFormatException x){
                       btnAdd.setEnabled(false);
                   }
                   finally {
                       btnAdd.setEnabled(true);
                   }
            }


        });





        btnSub.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String str = text.getText();
                    num1 = Double.parseDouble(text.getText());
                    text.setText("");
                    label.setText(str + "-");
                    buttonPoint.setEnabled(true);

                    selection = 2;
                }
                catch (NumberFormatException x){
                    btnSub.setEnabled(false);
                }
                finally {
                    btnSub.setEnabled(true);
                }

            }
        });
        btnMultiply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String str = text.getText();
                    num1 = Double.parseDouble(text.getText());
                    text.setText("");
                    label.setText(str + "*");

                    buttonPoint.setEnabled(true);

                    selection = 3;
                }
                catch (NumberFormatException x){
                    btnMultiply.setEnabled(false);
                }
                finally {
                    btnMultiply.setEnabled(true);
                }

            }
        });
        btnDivide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String str = text.getText();
                    num1 = Double.parseDouble(text.getText());
                    text.setText("");
                    label.setText(str + "/");

                    buttonPoint.setEnabled(true);

                    selection = 4;
                }
                catch (NumberFormatException x){
                    btnDivide.setEnabled(false);
                }
                finally {
                    btnDivide.setEnabled(true);
                }

            }
        });



        btnEquals.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                calculate();
            }
        });


    }



    private void calculate() {
        num2=Double.parseDouble(text.getText());
        label.setText(label.getText()+text.getText()+"=");

        switch(selection){
            case 1:{

                addMethod(num1, num2);
                //System.out.println(result);
                if(result%1!=0){

                    text.setText(Double.toString(result));

                }
                else{

                    int value=(int)result;
                    text.setText(String.valueOf(value));
                }
                buttonPoint.setEnabled(true);

                break;
            }

            case 2:{
                subMethod(num1, num2);
                if(result%1!=0){
                    text.setText(Double.toString(result));
                }
                else{
                    int value=(int)result;
                    text.setText(String.valueOf(value));
                }

                break;
            }
            case 3:{
                multiplyMethod(num1, num2);
                if(result%1!=0){
                    text.setText(Double.toString(result));
                }
                else{
                    int value=(int)result;
                    text.setText(String.valueOf(value));
                }

                break;
            }
            case 4:{
                divisionMethod(num1, num2);
                if(result%1!=0){
                    text.setText(Double.toString(result));
                }
                else{
                    int value=(int)result;
                    text.setText(String.valueOf(value));
                }

                break;
            }
            default:
                break;

        }
    }


    public double addMethod(double a, double b){
        result=a+b;
        return result;
    }

    public double subMethod(double a, double b){
        result=a-b;
        return  result;
    }
    public double multiplyMethod(double a, double b){
        result=a*b;
        return result;
    }
    public double divisionMethod(double a, double b){
        result=a/b;
        return result;
    }



    public static void main(String[] args) {
        JMenuBar menuBar;
        JMenu jMenu;
        JMenu jMenu2;
        JFrame frame = new JFrame("Calculator");
        menuBar=new JMenuBar();
        menuBar.setVisible(true);
        jMenu=new JMenu("File");
        jMenu2=new JMenu("Help");
        var exit=new JMenuItem("Exit");
        exit.addActionListener((event) -> System.exit(0));
        var MenuItem2=new JMenuItem("About");

        frame.setContentPane(new Calculator().Calculator);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setJMenuBar(menuBar);
        menuBar.add(jMenu);
        menuBar.add(jMenu2);
        jMenu.add(exit);
        jMenu2.add(MenuItem2);



    }

}
